import React, { Component } from 'react'
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom';
import './App.css';
import LandingPage from './landingPage/LandingPage'
import AlumniProfile from './alumniProfile/AlumniProfile'
import BrowseAlumni from './browseAlumni/BrowseAlumni'
import AdminLogin from './adminLogin/AdminLogin'
import AlumniLogin from './alumniLogin/AlumniLogin'
import AdminMgmt from './adminMgmt/AdminMgmt'
import AlumniProfileMgmt from './alumniProfileMgmt/AlumniProfileMgmt'
import EditAlumniProfile from './editAlumProfile/EditAlumniProfile'
import AlumniRegister from './alumniRegister/AlumniRegister'
import Stats from './stats/Stats'
import Navbar from './navbar/Navbar'

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <Navbar />
          <Route exact path="/" component={LandingPage}/>
          <Route exact path="/alum" component={BrowseAlumni}/>
          <Route exact path='/alum/:alumId' component={AlumniProfile}/>
          <Route exact path='/adminLogin' component={AdminLogin}/>
          <Route exact path='/alumniLogin' component={AlumniLogin}/>
          <Route exact path='/adminMgmt' component={AdminMgmt}/>
          <Route exact path='/alumniProfile' component={AlumniProfile}/>
          <Route exact path='/alumniProfileMgmt' component={AlumniProfileMgmt}/>
          <Route exact path='/editAlumniProfile' component={EditAlumniProfile}/>
          <Route exact path='/stats' component={Stats}/>
          <Route exact path='/alumniRegister' component={AlumniRegister}/>
        </div>
      </Router>
    );
  }
}

export default App;
