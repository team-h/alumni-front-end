import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Col, Row, Panel, Button } from 'react-bootstrap'
import '../App.css';
import '../alumniProfile/AlumniProfile.css';


class AlumniProfile extends Component {
  constructor(props){
    super(props)
    this.state = {
      profile : {
        Fname : '', 
        Lname : '', 
        degree: '', 
        major : '', 
        gradyear: '', 
        companyschool: '', 
        location : '', 
        Photo : '', 
        linkedin : ''
      },
    }
  }

  async componentDidMount() {
    const alum_id = this.props.match.params.alumId
    fetch('/alumni_profile', {
        method: 'POST',
        headers: {
            user_id : alum_id
        },
    })
    .then((response) => {
        return response.json()
    }).then((responseData) => {
        if( responseData.success == true )
        {
            console.log(responseData.results)
            this.setState({profile: responseData.results})
        } else {
            // otherwise we should redirect them back to alumni browse page.
        }
        return responseData;
    }).catch(function(error) {
        console.log('Error occurred during fetch... is the back-end running?\n' + error)
    });
  }

  render() {
    const p = this.state.profile

    return (
      <Panel className="container-fluid AlumniProfileContainer">
        <div className="row">
          <Button href="/alum">Back to Browse</Button>
          <h1><strong>Alumni Profiles</strong></h1>
          <h1>   {   p.Fname + ' ' + p.Lname || ''}</h1>
        </div>
        <Row>
          <div className="col-xs-4">
            <img className="AlumniPic center-block" src={p.Photo || require("./blank-profile-picture.png")} alt="No profile picture found."/>
          </div>
          <Panel className="col-xs-8 AlumniDesc">
            <p><strong>Current Company or School: </strong>{p.companyschool}</p>
            <p><strong>Location: </strong>{p.location}</p>
            <p><strong>Major: </strong>{p.major}</p>
            <p><strong>Degree: </strong>{p.degree}</p>
            <p><strong>Graduation Year: </strong> {p.gradyear}</p>
            <p><strong>Links: </strong><Button bsStyle="primary" href={p.linkedin}>Connect on LinkedIn</Button></p>
          </Panel>
        </Row>
      </Panel>
    );
  }
}

export default AlumniProfile;
