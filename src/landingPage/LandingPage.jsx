import React, { Component } from 'react'
import { Row, Col, Thumbnail, Media, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import logo from './logo.svg';
import '../landingPage/landingPage.css'
import '../App.css';


const FeaturedAlumni = ({props}) => {
  return (
    <div>
      <Col smOffset={3} sm={6}>
        <Media className="FeaturedAlumni">
          <Media.Left>
            <Media.Heading>
              <strong>Featured Alumni: </strong>
              <strong>{props.Fname || ''} {props.Lname || ''} ({props.GradYear || ''})</strong>
            </Media.Heading>
            <img height={225} width={150} src={props.Photo} />
            <i>'NodeJS is better than C++, no question'</i>
          </Media.Left>
          <Media.Right className='FeaturedDescription'>
            <p>This is a description where you can assume the alumni did something of great significance. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed viverra ex metus. Aliquam non lectus tortor. Nulla elit velit, lobortis sit amet nunc quis, ultricies venenatis quam. Maecenas eget nisi ipsum. Cras dui nibh, mattis eget libero ut, tempus cursus nunc. Pellentesque vehicula bibendum purus in efficitur. Donec porta tellus mi, mattis gravida enim varius id. Duis dictum ex est, in fringilla justo egestas et. Curabitur sed lacinia neque. Donec ac lectus mattis, volutpat eros vel, suscipit sem. Mauris tempor odio risus, at viverra neque placerat non. Ut nec imperdiet nulla. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
            <Button bsStyle="primary">View Full Article</Button>{' '}<Button>Alumni Archive</Button>
          </Media.Right>
        </Media>
      </Col>
    </div>
  )
}

class LandingPage extends Component {
  constructor(props){
    super(props)
    this.state = {
      res: {
        results: {},
      },
      connectionStatus: false,
      featuredAlumProps: {
        featuredName: 'John Doe',
        featuredPicture: '',
        gradYear: '1998',
        description: 'This is a description where you can assume the alumni did something of great significance. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed viverra ex metus. Aliquam non lectus tortor. Nulla elit velit, lobortis sit amet nunc quis, ultricies venenatis quam. Maecenas eget nisi ipsum. Cras dui nibh, mattis eget libero ut, tempus cursus nunc. Pellentesque vehicula bibendum purus in efficitur. Donec porta tellus mi, mattis gravida enim varius id. Duis dictum ex est, in fringilla justo egestas et. Curabitur sed lacinia neque. Donec ac lectus mattis, volutpat eros vel, suscipit sem. Mauris tempor odio risus, at viverra neque placerat non. Ut nec imperdiet nulla. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.',
        quote: 'I automated my whole job years ago, I dont actually code anything anymore',
        articleLink: '',
      },
    };
  }

  async componentDidMount() {
    const timeout = new Promise((resolve, reject) => {
        setTimeout(reject, 300, 'Request timed out');
    });

    const request = await fetch('http://localhost:3001/');
    const resJson = await request.json()
    this.setState({res: resJson}, () => console.log('res json ~',resJson))

    const response = await Promise
        .race([timeout, request])
        .then(response => this.setState({connectionStatus: true}))
        .catch(error => this.setState({connectionStatus: false}));
  }

  render() {
    return (
      <div className="App">
        <Row>
          <div className="App-header">
            <u>Computer Science Alumni</u>
          </div>
        </Row>
        <Row className="hidden-xs" style={{'padding-top': '25px'}} ></Row>
        <Row>
          <FeaturedAlumni props={this.state.res.results || {}} />
        </Row>
        <Row>
          <Col smOffset={3} sm={6}>
            <hr/>
            <p>{this.state.connectionStatus ? "Database Connection Successful" : "Database Connection Failure"}</p>
          </Col>
        </Row>
      </div>
    );
  }
}

export default LandingPage;
