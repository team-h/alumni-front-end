import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Col, Row, Badge } from 'react-bootstrap'
import '../alumniProfileMgmt/alumniProfileMgmt.css'
import '../App.css';

class AlumniProfileManagement extends Component {
  constructor(props){
    super(props)
    this.state = {
      userId: 'USERID',
      messages: ['hello', 'welcome to this.', 'i have not seen you recently']
    }
  }
  render() {
    return (
      <div>
        <Row>
          <Col sm={6} smOffset={3}>
            <div className="WelcomeBanner">
              <h1>Welcome {this.state.userId}</h1>
            </div>
            <div className="Messages" onClick={() => alert(this.state.messages)}>
              <p>Messages: <Badge>{this.state.messages.length}</Badge></p>
            </div>
          </Col>
        </Row>
        <Row>
          <Col sm={6} smOffset={3}>
            <h3>Messages: </h3>
            {this.state.messages.map(message => {
              return(<p>{message}</p>)
            })}
          </Col>
        </Row>
      </div>
    );
  }
}

export default AlumniProfileManagement;
