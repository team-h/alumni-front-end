import React, { Component } from 'react'
import { Nav, Navbar, NavItem, Glyphicon } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { LinkContainer } from 'react-router-bootstrap'
import Cookies from 'universal-cookie';
import '../App.css'
const cookies = new Cookies();


class MyNavbar extends Component {
  constructor(props){
    super(props)
    this.state = {
      loggedin: 0,
    }
  }

  loginState = () => {
    if( cookies.get('token') != undefined ) {
        cookies.remove('token');
        cookies.remove('user_id');
    }
  }

  render() {
    return (
      <Navbar className="Navbar">
        <Navbar.Header>
          <Navbar.Brand>
            <Link to='/'>
              <span className="logo hidden-xs"><img src="https://i.imgur.com/dEYWnaI.png" /></span>
              <span className="logo hidden-xl hidden-lg hidden-md hidden-sm"><p>Computer Science Alumni</p></span>
            </Link>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav pullRight>
            <LinkContainer to="/stats">
              <NavItem><Glyphicon glyph="globe" /> Breakdown</NavItem>
            </LinkContainer>
            <LinkContainer to="/alum">
              <NavItem><Glyphicon glyph="list" /> Browse Alumni</NavItem>
            </LinkContainer>
            <LinkContainer to="/alumniLogin">
              <NavItem onClick={this.loginState}>
                <Glyphicon glyph="log-in" /> {!cookies.get('token')?'Log-in':'Log-out'}
              </NavItem>
            </LinkContainer>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}

export default MyNavbar;
