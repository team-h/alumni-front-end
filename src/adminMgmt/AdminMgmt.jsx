import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Col } from 'react-bootstrap'
import ReactDataGrid from 'react-data-grid';
import { ReactDataGridPlugins, Toolbar, Data, Formatters, Editors } from 'react-data-grid-addons'
import update from 'immutability-helper'
import '../App.css';
import Cookies from 'universal-cookie';
const { Selectors } = Data;
const { DropDownFormatter } = Formatters;
const { DropDownEditor } = Editors;
const cookies = new Cookies();

const degreeTypes = [
  { id: 'bs', value: 'BS', text: 'BS', title: 'BS' },
  { id: 'ms', value: 'MS', text: 'MS', title: 'MS' },
  { id: 'phd', value: 'PHD', text: 'PHD', title: 'PHD' }
]
const DegreeTypesEditor = <DropDownEditor options={degreeTypes}/>;
const DegreeTypesFormatter = <DropDownFormatter options={degreeTypes} value="BS"/>;

const majorTypes = [
  { id: 'Computer Science', value: 'Computer Science', text: 'Computer Science', title: 'Computer Science' },
  { id: 'Computer Engineering', value: 'Computer Engineering', text: 'Computer Engineering', title: 'Computer Engineering' },
  { id: 'Underwater Basket Weaving', value: 'Underwater Basket Weaving', text: 'Underwater Basket Weaving', title: 'Underwater Basket Weaving' }
]
const MajorTypesEditor = <DropDownEditor options={majorTypes}/>;
const MajorTypesFormatter = <DropDownFormatter options={majorTypes} value="BS"/>;

const binaryTypes = [
  { id: '0', value: '0', text: '0', title: '0' },
  { id: '1', value: '1', text: '1', title: '1' }
]
const BinaryTypesEditor = <DropDownEditor options={binaryTypes}/>;
const BinaryTypesFormatter = <DropDownFormatter options={binaryTypes} value="BS"/>;

class AdminMgmt extends React.Component {
  constructor(props, context) {
    super(props, context);
    this._columns = [
      {
        key: 'user_id',
        name: 'ID',
        width: 80
      },
      {
        key: 'fname',
        name: 'First',
        filterable: true,
        sortable: true,
        editable: true
      },
      {
        key: 'lname',
        name: 'Last',
        filterable: true,
        sortable: true,
        editable: true
      },
      {
        key: 'email',
        name: 'Email',
        filterable: true,
        sortable: true,
        editable: false
      },
      {
        key: 'major',
        name: 'Major',
        filterable: true,
        sortable: true,
        editable: true,
        width: 130,
        editor: MajorTypesEditor,
        formatter: MajorTypesFormatter
      },
      {
        key: 'degree',
        name: 'Degree',
        width : 80,
        filterable: true,
        sortable: true,
        editable: true,
        editor: DegreeTypesEditor,
        formatter: DegreeTypesFormatter
      },
      {
        key: 'gradyear',
        name: 'Grad Year',
        width : 80,
        filterable: true,
        sortable: true,
        editable: true
      },
      {
        key: 'companyschool',
        name: 'Company/School',
        filterable: true,
        sortable: true,
        editable: true
      },
      {
        key: 'location',
        name: 'Location',
        filterable: true,
        sortable: true,
        editable: true
      },
      {
        key: 'profilecreationdate',
        name: 'Creation Date',
        filterable: true,
        sortable: true
      },
      {
        key: 'aotm',
        name: 'AOTM',
        width : 80,
        filterable: true,
        sortable: true,
        editable: true,
        editor: BinaryTypesEditor,
        formatter: BinaryTypesFormatter
      },
      {
        key: 'verified',
        name: 'Verified',
        width : 80,
        filterable: true,
        sortable: true,
        editable: true,
        editor: BinaryTypesEditor,
        formatter: BinaryTypesFormatter
      },
      {
        key: 'thesis',
        name: 'Thesis',
        filterable: true,
        sortable: true,
        editable: true
      },
      {
        key: 'linkedin',
        name: 'Linked In',
        filterable: true,
        sortable: true,
        editable: true
      },
      {
        key: 'photo',
        name: 'Photo',
        filterable: true,
        sortable: true,
        editable: true
      },
      {
        key: 'lastupdate',
        name: 'Last Update',
        filterable: true,
        sortable: true
      },
    ];

    this.state = { 
        filters: {}, 
        sortColumn: null, 
        sortDirection: null,
        profiles : [],
        rows: [],
    };
  }

  createRows = () => {
    let rows = [];
    this.state.profiles.forEach( e => {
      rows.push({
        user_id: e.user_id,
        fname: e.Fname,
        lname: e.Lname,
        email : e.email,
        major: e.major,
        degree: e.degree,
        gradyear: e.gradyear,
        companyschool: e.companyschool,
        location : e.Location,
        profilecreationdate: e.profilecreationdate,
        aotm : e.aotm,
        verified : e.verified,
        thesis : e.thesis,
        linkedin : e.linkedin,
        photo : e.Photo,
        lastupdate : e.lastupdate,
      });
    });
    this.setState({rows, rows});
  };
  
  async componentDidMount() {
      fetch('/admin/admin_alumni_list', {
          method: 'GET',
          headers: {
              token: cookies.get('token')
          },
      }).then((response) => {
          return response.json()
      }).then((responseData) => {
          if( responseData.success === true ) {
            this.setState({profiles : responseData.results}, this.createRows)
          }
      }).catch(function(error) {
          console.log('Error occurred during fetch... is the back-end running?\n' + error)
      });
  };
  
  handleGridRowsUpdated = ({ fromRow, toRow, updated }) => {
    let rows = Selectors.getRows(this.state)
    var val = updated[Object.keys(updated)[0]]
    var col = Object.keys(updated)[0]
    var old_val = rows[fromRow][col]
    
    // Make sure value actually changed.
    if( old_val != val ) {
        for (let i = fromRow; i <= toRow; i++) {
          let rowToUpdate = rows[i];
          let updatedRow = update(rowToUpdate, {$merge: updated});
          rows[i] = updatedRow;
        }

        this.setState({ rows });

        fetch('/admin/update', {
            method: 'POST',
            headers: {
                user_id : rows[fromRow].user_id,
                val : btoa(val),
                col : btoa(col),
                token: cookies.get('token')
            },
        }).then((response) => {
            return response.json()
        }).then((responseData) => {
            if( responseData.success === true ) {
                // Whatever
            }
        }).catch(function(error) {
            console.log('Error occurred during fetch... is the back-end running?\n' + error)
        });
    }
  };

  getRows = () => {
    return Selectors.getRows(this.state);
  };

  getSize = () => {
    return this.getRows().length;
  };

  rowGetter = (rowIdx) => {
    const rows = this.getRows();
    return rows[rowIdx];
  };

  handleGridSort = (sortColumn, sortDirection) => {
    this.setState({ sortColumn: sortColumn, sortDirection: sortDirection });
  };

  handleFilterChange = (filter) => {
    let newFilters = Object.assign({}, this.state.filters);
    if (filter.filterTerm) {
      newFilters[filter.column.key] = filter;
    } else {
      delete newFilters[filter.column.key];
    }

    this.setState({ filters: newFilters });
  };

  onClearFilters = () => {
    this.setState({ filters: {} });
  };

  render() {
    return  (
      <ReactDataGrid
        onGridSort={this.handleGridSort}
        enableCellSelect={true}
        columns={this._columns}
        rowGetter={this.rowGetter}
        rowsCount={this.getSize()}
        minHeight={window.innerHeight-130}
        toolbar={<Toolbar enableFilter={true}/>}
        onAddFilter={this.handleFilterChange}
        onGridRowsUpdated={this.handleGridRowsUpdated}
        onClearFilters={this.onClearFilters} />);
  }
}

export default AdminMgmt;

