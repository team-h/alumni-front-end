import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Row, Col, FormGroup, ControlLabel, FormControl, Button } from 'react-bootstrap'
import '../editAlumProfile/editAlumniProfile.css'
import '../App.css';
import Cookies from 'universal-cookie';
const cookies = new Cookies();

const departmentList = [
  {'label': 'Computer Science', 'id': 'cs'},
  {'label': 'Math', 'id': 'math'},
  {'label': 'Computer Engineering', 'id': 'ce'},
  {'label': 'Electrical Engineering', 'id': 'ee'},
  {'label': 'Underwater Basket Weaving', 'id': '🗑'},
]

const FieldGroup = (props) => {
  return (
    <FormGroup controlId={props.id}>
      <ControlLabel>{props.label}</ControlLabel>
      <FormControl {...props} />
    </FormGroup>
  )
}

const SelectGroup = (props) => {
  return(
    <FormGroup controlId={props.id}>
      <ControlLabel>{props.label} {props.multiple ? '(Select all that apply)': ''}</ControlLabel>
      <FormControl componentClass="select" defaultValue={props.defaultValue} onChange={props.onChange}>
        {props.optionArr.map(e => {
            return(<option key={e.id} value={e.id}>{e.label}</option>)
          })}
      </FormControl>
    </FormGroup>
  )
}

class EditAlumniProfile extends Component {
  constructor(props){
    super(props)
    this.state = {
      picture: '',
      fName: 'John',
      lName: 'Smith',
      major: 'cs',
      minor: 'Electrical Engineering',
      location: 'San Francisco, CA',
      company: 'Google',
      gradSchool: 'Stanford',
      graduationYear: '2014',
      thesisName: 'Using Machine Learning to grade papers for me',
      thesisLink: 'http://mst.edu',
      linkedin: 'http://linkedin.com'
    }
  }
  
  async componentDidMount() {
    const alum_id = cookies.get('user_id');
    console.log(alum_id);
    
    fetch('/alumni_profile', {
        method: 'POST',
        headers: {
            user_id : alum_id
        },
    })
    .then((response) => {
        return response.json()
    }).then((responseData) => {
        if( responseData.success == true )
        {
            console.log(responseData.results)
            this.setState({profile: responseData.results})
        } else {
            // otherwise we should redirect them back to alumni browse page.
        }
        return responseData;
    }).catch(function(error) {
        console.log('Error occurred during fetch... is the back-end running?\n' + error)
    });
  }

  editField = event => {
    const fieldInfo = event.target
    this.setState({
      [fieldInfo.id]: fieldInfo.value
    })
  }

  submitField = event => {
    alert('Submit pressed 👍')
    event.preventDefault();
  }

  render() {
    return (
      <div>
        <Row className="hidden-xs" style={{'padding-top': '50px'}}></Row>
        <Row>
          <Col sm={6} smOffset={3}>
            <h1>Edit Your Profile:</h1>
              <form onSubmit={this.submitField}>
                <FieldGroup
                  id="picture"
                  type="file"
                  label="Picture"
                  value={this.state.picture}
                  onChange={this.editField}
                />
                <FieldGroup
                  id="fName"
                  type="text"
                  label="First Name"
                  value={this.state.fName}
                  onChange={this.editField}
                />
                <FieldGroup
                  id="lName"
                  type="text"
                  label="Last Name"
                  value={this.state.lName}
                  onChange={this.editField}
                />
                <SelectGroup
                  id="major"
                  label="Major"
                  defaultValue={this.state.major}
                  optionArr={departmentList}
                  onChange={this.editField}
                />
                <FieldGroup
                  id="minor"
                  label="Minor"
                  multiple={true}
                  value={this.state.minor}
                  onChange={this.editField}
                />
                <FieldGroup
                  id="location"
                  type="text"
                  label="Current Location"
                  value={this.state.location}
                  onChange={this.editField}
                />
                <FieldGroup
                  id="company"
                  type="text"
                  label="Company"
                  value={this.state.company}
                  onChange={this.editField}
                />
                <FieldGroup
                  id="gradSchool"
                  type="text"
                  label="Grad School"
                  value={this.state.gradSchool}
                  onChange={this.editField}
                />
                <FieldGroup
                  id="graduationYear"
                  type="text"
                  label="Graduation Year"
                  value={this.state.graduationYear}
                  onChange={this.editField}
                />
                <FieldGroup
                  id="thesisName"
                  type="text"
                  label="Thesis Name"
                  value={this.state.thesisName}
                  onChange={this.editField}
                />
                <FieldGroup
                  id="thesisLink"
                  type="text"
                  label="Thesis URL"
                  value={this.state.thesisLink}
                  onChange={this.editField}
                />
                <FieldGroup
                  id="linkedin"
                  type="text"
                  label="Linkedin Profile"
                  value={this.state.linkedin}
                  onChange={this.editField}
                />
                <Button type="submit" className="SubmitButton">Submit</Button>
              </form>
          </Col>
        </Row>
      </div>
    );
  }
}

export default EditAlumniProfile;
