import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import {  Col, Row, Panel, Button } from 'react-bootstrap'
import {withScriptjs, withGoogleMap, GoogleMap, Marker, InfoWindow} from "react-google-maps"
import {ReactBubbleChart} from 'react-bubble-chart'
import {Line, Pie} from "react-chartjs"
import '../App.css';
import '../stats/Stats.css';


const MyMapComponent = withScriptjs(withGoogleMap((props) =>
  <GoogleMap
    defaultZoom={6}
    defaultCenter={{ lat: 37.9485, lng: -91.7715 }}
  >
  {props.locations.map(e => {
    return <Marker position={{lat: parseFloat(e.lat), lng: parseFloat(e.lng)}}/>
  })}
  </GoogleMap>
));



var chartOptions = {responsive: true}


//Info window https://tomchentw.github.io/react-google-maps/#infowindow

class Stats extends Component {
  constructor(props){
    super(props)
    this.state = {
        locations: [],
        pieChartData:[],
        lineChartData1: {
          labels: [],
          datasets: [
              {
                  label: "Computer Engineering",
                  fillColor: "rgba(151,187,205,0.2)",
                  strokeColor: "rgba(151,187,205,1)",
                  pointColor: "rgba(151,187,205,1)",
                  pointStrokeColor: "#fff",
                  pointHighlightFill: "#fff",
                  pointHighlightStroke: "rgba(151,187,205,1)",
                  data: []
              }
          ]
        },
        lineChartData2: {
          labels: [],
          datasets: [
              {
                  label: "Computer Science",
                  fillColor: "rgba(151,187,205,0.2)",
                  strokeColor: "rgba(151,187,205,1)",
                  pointColor: "rgba(151,187,205,1)",
                  pointStrokeColor: "#fff",
                  pointHighlightFill: "#fff",
                  pointHighlightStroke: "rgba(151,187,205,1)",
                  data: []
              }
          ]
        }
    }
  }

  async componentDidMount() {

    const request = await fetch('http://localhost:3001/stats');
    const resJson = await request.json();

    console.log(resJson.results);

    var comp_names = Object.keys(resJson.results.comp);
    var comp_values = Object.values(resJson.results.comp);

    var cs_grad_years = Object.keys(resJson.results.cs);
    var cs_grad_amount = Object.values(resJson.results.cs);

    var cpe_grad_years = Object.keys(resJson.results.cpe);
    var cpe_grad_amount = Object.values(resJson.results.cpe);

    //var lines = cs_grad_years.concat(cpe_grad_years);
    //var line_labels = lines.filter(function (item, pos) {return lines.indexOf(item) == pos});

    var loc_names = Object.keys(resJson.results.loc);
    var loc_values = Object.values(resJson.results.loc);

    var pie = [
              {
                  value: comp_values[0],
                  color:"#F7464A",
                  highlight: "#FF5A5E",
                  label: comp_names[0]
              },
              {
                  value: comp_values[1],
                  color: "#46BFBD",
                  highlight: "#5AD3D1",
                  label: comp_names[1]
              },
              {
                  value: comp_values[2],
                  color: "#FDB45C",
                  highlight: "#FFC870",
                  label: comp_names[2]
              },
              {
                  value: comp_values[3],
                  color: "#949FB1",
                  highlight: "#A8B3C5",
                  label: comp_names[3]
              },
              {
                  value: comp_values[4],
                  color: "#4D5360",
                  highlight: "#616774",
                  label: comp_names[4]
              }
        ]

    var line1 = {
                labels: cpe_grad_years,
                datasets: [
                  {
                      data: cpe_grad_amount
                  }
                ]
            }

      var line2 = {
        labels: cs_grad_years,
        datasets: [
          {
              data: cs_grad_amount
          },
        ]
      }

    const locationsArray = await fetch('http://localhost:3001/stats/locations')
    const locationsJson = await locationsArray.json()

    this.setState({
      pieChartData: pie,
      lineChartData1: line1,
      lineChartData2: line2,
      locations: locationsJson.results,
    })
  }

  render() {
    return (
      <Panel className="StatsContainer" header="Alumni Statistics">
        <Row>
           <div className="col-xs-6">
             <h3>Computer Engineering Grads by Year</h3>
             <Line data={this.state.lineChartData1} options={chartOptions} />
           </div>
          <div className="col-xs-6">
             <h3>Computer Science Grads by Year</h3>
             <Line data={this.state.lineChartData2} options={chartOptions}/>
          </div>

        </Row>
        <h3>Explore Alumni Map</h3>
        <MyMapComponent
          googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyC4R6AN7SmujjPUIGKdyao2Kqitzr1kiRg&v=3.exp&libraries=geometry,drawing,places"
          loadingElement={<div style={{ height: `100%` }} />}
          containerElement={<div style={{ height: `400px` }} />}
          mapElement={<div style={{ height: `100%` }} />}
          locations={this.state.locations}
         />

         <div>
            <h3>Top Careers for Our Grads</h3>
            <Pie data={this.state.pieChartData} options={chartOptions}/>
         </div>

      </Panel>
    );
  }
}

export default Stats;
