import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Col, FormGroup, FormControl, ControlLabel, Button, Checkbox } from 'react-bootstrap'
import PasswordMask from 'react-password-mask';
import Cookies from 'universal-cookie';
import '../App.css';
import '../alumniLogin/AlumniLogin.css';
const cookies = new Cookies();

class AlumniLogin extends Component {
  constructor(props) {
    super(props);
    this.state = {
        success: null,
        email: '',
        password: ''
    };
  }

  gotEmail(event){
      event.preventDefault();
      fetch('/authenticate', {
          method: 'POST',
          headers: {
              Authorization : btoa(JSON.stringify({'email' : this.state.email.value, 'password' : this.state.password.value}))
          },
      })
      .then((response) => {
          return response.json()
      }).then((responseData) => {
          if( responseData.success == true )
          {
              console.log( "login token: ", responseData.token );
              cookies.set('token', responseData.token, { path: '/' });
              cookies.set('user_id', responseData.user_id, { path: '/' });
              this.setState({success : null})
              // Redirect to landing page upon successful login, adminMgmt if admin...
              if( responseData.isadmin == 1 ) {
                this.props.history.push('/adminMgmt');
              } else {
                this.props.history.push('/');
              }
          } else {
              this.setState({success : 'error'})
              console.log('Login Error: ' + responseData.error)
          }
          return responseData;
      }).catch(function(error) {
          console.log('Error occurred during fetch...\n' + error)
      });
  }
    
  getValidationState() {
      return this.state.success;
  }

  render() {
    return (
      <div className="Login">
        <form onSubmit={this.gotEmail.bind(this)}>
            <FormGroup controlId="formHorizontalEmail">
                <Col componentClass={ControlLabel} sm={2} className="emailEnter">
                    Email
                </Col>
                <Col sm={10}>
                    <FormControl type="email" placeholder="Email" inputRef={ref => {this.state.email = ref}}/>
                </Col>
            </FormGroup>

            <FormGroup controlId="formHorizontalPassword" validationState={this.getValidationState()}>
                <Col componentClass={ControlLabel} sm={2} className="pwEnter">
                    Password
                </Col>
                <Col sm={10}>
                    <FormControl type="password" placeholder="Password" inputRef={ref => {this.state.password = ref}}/>
                </Col>
            </FormGroup>

            <FormGroup>
                <Col smOffset={2} sm={3} className="submitButton">
                    <Button type="submit">
                        Sign in
                    </Button>
                </Col>
            </FormGroup>

            <FormGroup>
                <Col smOffset={2} sm={4} className="registerLink">
                    <Link to={`/alumniRegister`}>
                        New User? Register Here
                    </Link>
                </Col>
            </FormGroup>

        </form>
      </div>
    );
  }
}

export default AlumniLogin;

