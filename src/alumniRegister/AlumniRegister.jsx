import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { withRouter } from 'react-router';
import { Col, FormGroup, FormControl, ControlLabel, Button, Checkbox } from 'react-bootstrap'
import PasswordMask from 'react-password-mask';
import '../App.css';
import '../alumniRegister/AlumniRegister.css';

class alumniRegister extends Component {
  constructor(props) {
    super(props);
    this.state = {
        email               : '',
        password            : '',
        confirm_pswd        : '',
        fname               : '',
        lname               : '',
        companyschool       : '',
        location            : '',
        major               : 'Computer Science',
        degree              : 'BS',
        minor               : '',
        gradyear            : '',
        thesis              : '',
        linkedin            : '',
        photo               : null,
        latitude            : '',
        longitude           : '',
    };
  }

  getValidationState(input) {
      var length = 0
      switch(input) {
          case 'password':
              length = this.state.password.length
              if (length > 8) return 'success';
              else if (length > 0) return 'error';
              else return null;
          case 'confirm_pswd':
              length = this.state.confirm_pswd.length
              if(this.state.password != this.state.confirm_pswd && length != 0) return 'error';
              else if (length == 0) return null;
              else if (this.state.password == this.state.confirm_pswd) return 'success';
              else return null;
          default:
              return null;
      }
  }

  handleChange(event) {
      var state = {};
      state[event.target.name] = event.target.value
      this.setState(state)
  }


  formSubmission(event){
      event.preventDefault();

      console.log(this.state)

      const urlSafeAddress = this.state.location.split(' ').join('+')
      fetch(
        `https://maps.googleapis.com/maps/api/geocode/json?address=${urlSafeAddress}&key=AIzaSyB1nVAXGwttJW5dzQelTioxCxOFLYxJf1o`
      ).then((response) => {
        return response.json()
      }).then((data) => {
        // console.log(data.results[0].geometry.location.lat)
        const latResponse = data.results[0].geometry.location.lat
        const longResponse = data.results[0].geometry.location.lng
        console.log('lat long', latResponse, longResponse)
        this.setState({
          latitude: latResponse,
          longitude: longResponse,
        })
      }).then(() => {
        fetch('/register', {
            method: 'POST',
            headers: {
                registration : btoa(JSON.stringify(this.state))
            },
        })
        .then((response) => {
            return response.json()
        }).then((responseData) => {
            console.log( responseData )
            if( responseData.success == true )
            {
                this.props.history.push('/');
            } else {
            // tell them what went wrong...
            }
            return responseData;
        }).catch(function(error) {
            console.log('Error occurred during fetch... is the back-end running?\n' + error)
        });
      })
  }

  render() {
    return (
      <div className="Register">
        <form horizontal onSubmit={this.formSubmission.bind(this)}>

            <FormGroup controlId="formHorizontalEmail">
                <Col componentClass={ControlLabel} sm={2} className="emailEnter">
                    Email
                </Col>
                <Col sm={10}>
                    <FormControl
                        type="email"
                        name="email"
                        required
                        placeholder="Email"
                        maxLength="45"
                        value={this.state.email}
                        onChange={this.handleChange.bind(this)}>
                    </FormControl>
                </Col>
            </FormGroup>

            <FormGroup controlId="formHorizontalPassword" validationState={this.getValidationState('password')}>
                <Col componentClass={ControlLabel} sm={2} className="pwEnter">
                    Password
                </Col>
                <Col sm={10}>
                    <FormControl
                        type="password"
                        placeholder="Password"
                        name="password"
                        required
                        maxLength="45"
                        pattern=".{8,}"
                        title = "The password must contain 8 or more characters..."
                        value={this.state.password}
                        onChange={this.handleChange.bind(this)}>
                    </FormControl>
                </Col>
            </FormGroup>

            <FormGroup controlId="formHorizontalConfirmPassword" validationState={this.getValidationState('confirm_pswd')}>
                <Col componentClass={ControlLabel} sm={2} className="confirmpwEnter">
                    Confirm Password
                </Col>
                <Col sm={10}>
                    <FormControl
                        type="password"
                        placeholder="Password"
                        required
                        name="confirm_pswd"
                        maxLength="45"
                        value={this.state.confirm_pswd}
                        onChange={this.handleChange.bind(this)}>
                    </FormControl>
                </Col>
            </FormGroup>

            <FormGroup controlId="formHorizontalFname">
                <Col componentClass={ControlLabel} sm={2} className="fnameEnter">
                    First Name
                </Col>
                <Col sm={10}>
                    <FormControl
                        type="text"
                        placeholder="First Name"
                        name="fname"
                        maxLength="45"
                        pattern="[A-Za-z]{2,}"
                        required
                        title = "Name may only contain letters and must contain at least two characters."
                        value={this.state.fname}
                        onChange={this.handleChange.bind(this)}>
                    </FormControl>
                </Col>
            </FormGroup>

            <FormGroup controlId="formHorizontalLname">
                <Col componentClass={ControlLabel} sm={2} className="lnameEnter">
                    Last Name
                </Col>
                <Col sm={10}>
                    <FormControl
                        type="text"
                        placeholder="Last Name"
                        name="lname"
                        pattern="[ A-Za-z0-9]{2,}"
                        required title = "Name may only contain letters and must contain at least two characters."
                        value={this.state.lname}
                        onChange={this.handleChange.bind(this)}>
                    </FormControl>
                </Col>
            </FormGroup>

            <FormGroup controlId="formHorizontalInstitution">
                <Col componentClass={ControlLabel} sm={2} className="institutionEnter">
                    Current Company or School
                </Col>
                <Col sm={10}>
                    <FormControl
                        type="text"
                        placeholder="Company / School"
                        maxLength="45"
                        name="companyschool"
                        pattern="[ A-Za-z]{3,}"
                        required
                        title = "This shouldn't be empty"
                        value={this.state.companyschool}
                        onChange={this.handleChange.bind(this)}>
                    </FormControl>
                </Col>
            </FormGroup>

            <FormGroup controlId="formHorizontalLocation">
                <Col componentClass={ControlLabel} sm={2} className="locationEnter">
                    Location
                </Col>
                <Col sm={10}>
                    <FormControl
                        type="text"
                        placeholder="Location"
                        name="location"
                        maxLength="45"
                        pattern="[ A-Za-z,]{4,}"
                        required
                        title = "Name may only contain letters, commas, and spaces."
                        value={this.state.location}
                        onChange={this.handleChange.bind(this)}>
                    </FormControl>
                </Col>
            </FormGroup>

            <FormGroup controlId="formHorizontalMajor">
              <Col componentClass={ControlLabel} sm={2} className="majorEnter">
                    Major
              </Col>
              <Col sm={10}>
                  <FormControl
                    componentClass="select"
                    placeholder="Computer Science"
                    name="major"
                    value={this.state.major}
                    onChange={this.handleChange.bind(this)}>
                    <option value="Computer Science">Computer Science</option>
                    <option value="Computer Engineering">Computer Engineering</option>
                    <option value="Underwater Basket Weaving">Underwater Basket Weaving</option>
                  </FormControl>
              </Col>
            </FormGroup>

            <FormGroup controlId="formHorizontalDegree">
              <Col componentClass={ControlLabel} sm={2} className="degreeEnter">
                    Degree
              </Col>
              <Col sm={10}>
                  <FormControl
                    componentClass="select"
                    placeholder="BS"
                    name="degree"
                    value={this.state.degree}
                    onChange={this.handleChange.bind(this)}>
                    <option value="BS">BS</option>
                    <option value="MS">MS</option>
                    <option value="PHD">PHD</option>
                  </FormControl>
              </Col>
            </FormGroup>

            <FormGroup controlId="formHorizontalMinor">
                <Col componentClass={ControlLabel} sm={2} className="minorEnter">
                    Minor
                </Col>
                <Col sm={10}>
                    <FormControl
                        type="text"
                        placeholder="Minor (Optional)"
                        name="minor"
                        maxLength="45"
                        pattern="[ A-Za-z]{2,}"
                        title = "Name may only contain letters and spaces."
                        value={this.state.minor}
                        onChange={this.handleChange.bind(this)}>
                    </FormControl>
                </Col>
            </FormGroup>

            <FormGroup controlId="formHorizontalGradYear">
                <Col componentClass={ControlLabel} sm={2} className="gradYearEnter">
                    Graduation Year
                </Col>
                <Col sm={10}>
                    <FormControl
                        type="number"
                        min="1920"
                        max="2070"
                        required
                        placeholder="Grad Year"
                        name="gradyear"
                        value={this.state.gradyear}
                        onChange={this.handleChange.bind(this)}>
                    </FormControl>
                </Col>
            </FormGroup>

            <FormGroup controlId="formHorizontalThesis">
                <Col componentClass={ControlLabel} sm={2} className="thesisEnter">
                    Thesis
                </Col>
                <Col sm={10}>
                    <FormControl
                        type="text"
                        placeholder="Thesis Title"
                        name="thesis"
                        maxLength="45"
                        pattern="[ A-Za-z]{4,}"
                        title = "Name may only contain letters, numbers, and spaces."
                        value={this.state.thesis}
                        onChange={this.handleChange.bind(this)}>
                    </FormControl>
                </Col>
            </FormGroup>

            <FormGroup controlId="formHorizontalFileselect">
                <Col componentClass={ControlLabel} sm={2} className="linkedinEnter">
                    Photo
                </Col>
                <Col sm={10}>
                    <FormControl
                        name="photo"
                        type="file"
                        label="File"
                        help="Select a photo."
                        accept="image/x-png,image/gif,image/jpeg"
                        value={this.state.photo}
                        onChange={this.handleChange.bind(this)}>
                    </FormControl>
                </Col>
            </FormGroup>

            <FormGroup controlId="formHorizontalLinkedin">
                <Col componentClass={ControlLabel} sm={2} className="linkedinEnter">
                    LinkedIn
                </Col>
                <Col sm={10}>
                    <FormControl
                        type="url"
                        placeholder="LinkedIn"
                        name="linkedin"
                        maxLength="128"
                        required
                        value={this.state.linkedin}
                        onChange={this.handleChange.bind(this)}>
                    </FormControl>
                </Col>
            </FormGroup>

            <FormGroup>
                <Col smOffset={2} sm={10} className="submitButton">
                    <Button type="submit">
                        Register
                    </Button>
                </Col>
            </FormGroup>

        </form>
      </div>
    );
  }
}

export default alumniRegister;
