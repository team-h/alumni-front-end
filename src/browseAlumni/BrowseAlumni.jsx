import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Row, Col, Button, FormGroup, FormControl, Panel } from 'react-bootstrap'
import '../App.css';

class BrowseAlumni extends Component {
  constructor(props) {
    super(props)
    this.state = {
      validationState: null,
      searchInput : '',
      resList : [],
      list : [],
    }
  }

  async componentDidMount() {
      fetch('/alumni_list'
      ).then((response) => {
          return response.json()
      }).then((responseData) => {
          if( responseData.success === true ) {
            this.setState({list : responseData.results})
          }
      }).catch(function(error) {
          console.log('Error occurred during fetch... is the back-end running?\n' + error)
      });
  }

  async search() {
    try {
      const res = await fetch(`http://localhost:3001/search/${this.state.searchInput}`)
      const res_json = await res.json()

      if(res_json.results.length < 1){
        this.setState({validationState: "error"})
      } else {
        this.setState({
          resList: res_json.results,
          validationState: null,
        })
      }

    } catch (e) {
      console.log(e)
      this.setState({resList: []})
    }
  }

  handleChange = (e) => {
    this.setState({ searchInput: e.target.value });
  }

  clear = () => {
    this.setState({
      resList: [],
      searchInput: '',
      validationState: null,
    })
  }

  renderAlumList(){
    const rows = []

    if(this.state.resList.length < 1) {
      var lazyList = this.state.list
    } else {
      var lazyList = this.state.resList
    }

    lazyList.forEach( e => {
      rows.push(
        <div key={e.user_id} className='BrowseAlumniPanel'>
          <Panel header={
              <Link to={`/alum/${e.user_id}`}>
                {e.Fname + ' ' + e.Lname}
              </Link>}>
            <p>Graduated: {e.gradyear}</p>
            {e.companyschool ? <p>Works at {e.companyschool}</p> : <p>Unemployed</p>}
          </Panel>
        </div>
      )
    })

    return rows
  }

  render(){
    return (
      <div>
        <Row className="hidden-xs" style={{'padding-top': '50px'}} ></Row>
        <Row>
          <Col smPush={2} sm={6}>
            <FormGroup validationState={this.state.validationState}>
              <FormControl type="text"
                placeholder="Search by Alumni Name"
                onChange={this.handleChange}
                value={this.state.searchInput}/>
            </FormGroup>
          </Col>
          <Col smPush={2} sm={2}>
            <Button onClick={() => this.search()}>Search</Button>{' '}
            <Button bsStyle="warning" onClick={this.clear}>Clear</Button>
          </Col>
        </Row>
        <Row>
          <Col smPush={2} sm={8}>
            {this.renderAlumList()}
          </Col>
        </Row>
      </div>
    )
  }
}

export default BrowseAlumni
