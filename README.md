MST Computer Science Alumni page (front-end)

# How to setup
1. Install the most recent verison of https://nodejs.org/en/download/
2. Git clone this repository into a folder on your computer
3. Run 'npm install' in the directory, this takes a few minutes
4. Run 'npm start' to start the server

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Below you will find some information on how to perform common tasks.<br>
You can find the most recent version of this guide [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).
